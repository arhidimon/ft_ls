cmake_minimum_required(VERSION 3.8)
project(ft_ls)

set(LIBFT ${CMAKE_SOURCE_DIR}/libft/libft.a)
set(CMAKE_C_STANDARD 99)

set(SOURCE_FILES src/main.c src/get_options.c include/ft_ls.h src/qsort.c src/appendstr.c libft/ft_memset.c libft/ft_strchr.c libft/ft_strcmp.c libft/ft_strjoin.c
        libft/ft_strcpy.c libft/ft_strcat.c
        libft/ft_strdup.c libft/ft_strlen.c libft/ft_putstr.c libft/ft_putendl.c src/printlong.c src/get_file_type.c src/get_files_in_dir.c src/get_only_visible.c src/recursive_search.c
        libft/ft_putchar.c
		src/print_1col.c src/get_strarr_len.c src/get_fullname.c src/add_file.c src/compares.c src/get_dir.c src/get_num_files.c)
#target_link_libraries(ft_ls ${LIBFT})
add_executable(ft_ls ${SOURCE_FILES})