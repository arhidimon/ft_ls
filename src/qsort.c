/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   qsort.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dbezruch <dbezruch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/15 18:29:00 by dbezruch          #+#    #+#             */
/*   Updated: 2017/12/15 18:29:00 by dbezruch ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#include <stdlib.h>
#include "../include/ft_ls.h"
void swap_str_ptrs(char const **arg1, char const **arg2)
{
	const char *tmp = *arg1;
	*arg1 = *arg2;
	*arg2 = tmp;
}

void quicksort_strs(char const *args[], unsigned int len)
{
	unsigned int i, pvt=0;

	if (len <= 1)
		return;

	// swap a randomly selected value to the last node
	swap_str_ptrs(args+((unsigned int)rand() % len), args+len-1);

	// reset the pivot index to zero, then scan
	for (i=0;i<len-1;++i)
	{
		if (ft_strcmp(args[i], args[len-1]) < 0)
			swap_str_ptrs(args+i, args+pvt++);
	}

	// move the pivot value into its place
	swap_str_ptrs(args+pvt, args+len-1);

	// and invoke on the subsequences. does NOT include the pivot-slot
	quicksort_strs(args, pvt++);
	quicksort_strs(args+pvt, len - pvt);
}

void swap_files_ptrs(t_file **arg1, t_file **arg2)
{
	t_file *tmp = *arg1;
	*arg1 = *arg2;
	*arg2 = tmp;
}

void quicksort_files(t_file **args, unsigned int len,int (*cmp)(t_file*, t_file*))
{
	unsigned int i, pvt=0;

	if (len <= 1)
		return;

	// swap a randomly selected value to the last node
	swap_files_ptrs(args+((unsigned int)rand() % len), args+len-1);

	// reset the pivot index to zero, then scan
	for (i=0;i<len-1;++i)
	{
		if (cmp(args[i], args[len-1]) < 0)
			swap_files_ptrs(args+i, args+pvt++);
	}

	// move the pivot value into its place
	swap_files_ptrs(args+pvt, args+len-1);

	// and invoke on the subsequences. does NOT include the pivot-slot
	quicksort_files(args, pvt++, cmp);
	quicksort_files(args+pvt, len - pvt, cmp);
}