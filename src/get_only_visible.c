/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_only_visible.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dbezruch <dbezruch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/19 19:26:00 by dbezruch          #+#    #+#             */
/*   Updated: 2017/12/19 19:26:00 by dbezruch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/ft_ls.h"

t_file **get_only_visible(t_file **files)
{
	t_file **resstrings;

	resstrings = NULL;
	if (!files)
		return (NULL);
	while (*files)
	{
		if ((*files)->name && *((*files)->name) != '.')
			add_file_copy(&resstrings, *files);
		files++;
	}
	return (resstrings);
}