/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_options.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dbezruch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/12 17:13:00 by dbezruch          #+#    #+#             */
/*   Updated: 2018/10/23 14:58:08 by dbezruch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#include "../include/ft_ls.h"

void set_option(char opt)
{
	g_a.p.a = ((opt == 'a') ? 1 : g_a.p.a);
	g_a.p.l = ((opt == 'l') ? 1 : g_a.p.l);
	g_a.p.one = ((opt == '1') ? 1 : g_a.p.one);
	g_a.p.r = ((opt == 'r') ? 1 : g_a.p.r);
	g_a.p.t = ((opt == 't') ? 1 : g_a.p.t);
	g_a.p.u = ((opt == 'u') ? 1 : g_a.p.u);
	g_a.p.f = ((opt == 'f') ? 1 : g_a.p.f);
	g_a.p.g = ((opt == 'g') ? 1 : g_a.p.g);
	g_a.p.d = ((opt == 'd') ? 1 : g_a.p.d);
	g_a.p.G = ((opt == 'G') ? 1 : g_a.p.G);
	g_a.p.R = ((opt == 'R') ? 1 : g_a.p.R);
}

int get_options(int argc, char **argv, char *av_opt, char **opt) {
	int		i;
	int 	j;

	i = 0;
	while (i++ < argc - 1)
	{
		if (argv[i][0] != '-')
			break;
		else if (argv[i][1] == '-')
			return (argv[i][2] == '\0' ? i : -1);
		j = 1;
		while (argv[i][j] && ft_strchr(av_opt, argv[i][j]))
		{
				set_option(argv[i][j]);
			j++;
		}
		if (argv[i][j] && !ft_strchr(av_opt, argv[i][j]))
			return (-1);
	}
	return (i);
}
