/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   appendstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dbezruch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/15 18:44:00 by dbezruch          #+#    #+#             */
/*   Updated: 2018/10/23 14:57:34 by dbezruch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "../include/ft_ls.h"

void appendstr(char ***array, char *str)
{
	int i;
	char **newarray;
	i = 0;
	if (*array)
	{
		while ((*array)[i])
			i++;
	}
	newarray = (char**)malloc(sizeof(char*)*(i + 2));
	if (*array)
	{
		i = -1;
		while ((*array)[++i])
			newarray[i] = (*array)[i];
		free(*array);
	}
	newarray[i] = ft_strdup(str);
	newarray[i + 1] = NULL;
	*array = newarray;
}