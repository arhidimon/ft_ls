/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_strarr_len.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dbezruch <dbezruch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/20 18:20:00 by dbezruch          #+#    #+#             */
/*   Updated: 2017/12/20 18:20:00 by dbezruch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/ft_ls.h"

int get_strarr_len(char **string)
{
	int i;

	if (!string)
		return (-1);
	i = 0;
	while (string[i])
		i++;
	return (i);
}