/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   recursive_search.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dbezruch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/20 15:34:00 by dbezruch          #+#    #+#             */
/*   Updated: 2018/10/23 14:56:56 by dbezruch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/ft_ls.h"
#include <sys/stat.h>
#include <stdlib.h>

void recursive_search(char *startdir,char *curdir, t_file** (*get_dir_content)(char *), void (*printfunc)(char *, t_file **))
{
	t_file **files;
	char *t;

	if (ft_strcmp(startdir, curdir))
	{
		ft_putstr("\n");
		ft_putstr(curdir);
		ft_putstr(":\n");
	}
	files = get_only_visible((*get_dir_content)(curdir));
	if (get_num_files(files) > 0)
	{
		quicksort_files(files, get_num_files(files), &cmp_name);
		(*printfunc)(curdir, files);
		while (*files) {

			if (S_ISDIR((*files)->st.st_mode) && ((*files)->st.st_mode & S_IRUSR) && ft_strcmp((*files)->name, ".") && ft_strcmp((*files)->name, ".."))
			{
				t = get_fullname(curdir, (*files)->name);
				recursive_search(startdir, t, get_dir_content, printfunc);
				free(t);
			}
			files++;
		}
	}
	//free(strings);
}