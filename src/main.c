/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dbezruch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/18 15:55:50 by dbezruch          #+#    #+#             */
/*   Updated: 2018/10/23 14:57:09 by dbezruch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/ft_ls.h"
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <dirent.h>

t_app g_a;

int main(int argc, char **argv) {
	char	*opt;
	int		code;
	DIR *dir;
	struct dirent *entry;
	char **strings;
	char **visible;

	int i;

	if ((code = get_options(argc, argv, "1lAaRrtufgd", &opt)) < 0)
		return 0;

	printf("[%i]options:|%s|\n",code, opt);


	//strings = get_files_in_dir(argv[code]);
	//quicksort_strs(strings, 8);
	//visible = get_only_visible(strings);
	i = 0;

//	 **files, **vfiles;
//	files = get_dir("./");
//	vfiles = get_only_visible(files);
//	printlong("./", vfiles);

	//recursive_search(argv[code], argv[code],&get_dir, &printlong);

	//closedir(dir);
	if (opt)
		free(opt);
    return 0;
}
