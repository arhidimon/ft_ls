/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_files_in_dir.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dbezruch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/19 17:49:00 by dbezruch          #+#    #+#             */
/*   Updated: 2018/10/23 12:09:08 by dbezruch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/ft_ls.h"
#include <stdio.h>
#include <dirent.h>

char **get_files_in_dir(char *dirname)
{
	DIR *dir;
	struct dirent *entry;
	char **strings;

	strings = NULL;
	dir = opendir(dirname);
	if (!dir)
	{
		perror(P_NAME);
		return (NULL);
	}
	while ((entry = readdir(dir)) != NULL)
		appendstr(&strings, entry->d_name);
	closedir(dir);
	return (strings);
}