/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_fullname.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dbezruch <dbezruch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/21 13:57:00 by dbezruch          #+#    #+#             */
/*   Updated: 2017/12/21 13:57:00 by dbezruch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/ft_ls.h"
#include <stdlib.h>

char *get_fullname(char *dir, char *name)
{
	size_t	dirsize;
	size_t	namesize;
	char*	fullname;

	dirsize = ft_strlen(dir);
	namesize = ft_strlen(name);
	fullname = (char*)malloc(dirsize+namesize+2);
	ft_strcpy(fullname, dir);
	fullname[dirsize] = '/';
	ft_strcpy(fullname + dirsize + 1, name);
	fullname[dirsize+namesize+1] = '\0';
	return (fullname);
}