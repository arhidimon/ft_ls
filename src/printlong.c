/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   printlong.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dbezruch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/19 15:38:00 by dbezruch          #+#    #+#             */
/*   Updated: 2018/10/23 14:56:56 by dbezruch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/ft_ls.h"
#include <sys/stat.h>

void printlong(char *curdir, t_file **files)
{
	int i;
	
	i = 0;
	while (files[i])
	{
		ft_putchar((S_ISDIR(files[i]->st.st_mode)) ? 'd' : '-');
		ft_putchar((files[i]->st.st_mode & S_IRUSR) ? 'r' : '-');
		ft_putchar((files[i]->st.st_mode & S_IWUSR) ? 'w' : '-');
		ft_putchar((files[i]->st.st_mode & S_IXUSR) ? 'x' : '-');
		ft_putchar((files[i]->st.st_mode & S_IRGRP) ? 'r' : '-');
		ft_putchar((files[i]->st.st_mode & S_IWGRP) ? 'w' : '-');
		ft_putchar((files[i]->st.st_mode & S_IXGRP) ? 'x' : '-');
		ft_putchar((files[i]->st.st_mode & S_IROTH) ? 'r' : '-');
		ft_putchar((files[i]->st.st_mode & S_IWOTH) ? 'w' : '-');
		ft_putchar((files[i]->st.st_mode & S_IXOTH) ? 'x' : '-');
		ft_putstr("   ");
		ft_putendl(files[i++]->name);
	}
}
