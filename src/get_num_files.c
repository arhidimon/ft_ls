/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_num_files.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dbezruch <dbezruch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/26 14:43:00 by dbezruch          #+#    #+#             */
/*   Updated: 2017/12/26 14:43:00 by dbezruch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/ft_ls.h"

unsigned int get_num_files(t_file **files)
{
	int i;

	if (!files)
		return (0);
	i = 0;
	while (files[i])
		i++;
	return (i);
}