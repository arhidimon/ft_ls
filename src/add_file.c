/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   add_file.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dbezruch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/26 13:02:00 by dbezruch          #+#    #+#             */
/*   Updated: 2018/10/23 14:57:34 by dbezruch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "../include/ft_ls.h"
#include <sys/stat.h>

t_file	*new_file(char *dir, char *name)
{
	char *t;

	t_file	*file;
	if (!(file = malloc(sizeof(t_file))))
		return (file);
	file->name = ft_strdup(name);
	t = get_fullname(dir, name);
	stat(t, &file->st);
	free(t);
	return (file);
}

t_file	*copy_file(t_file *file)
{
	t_file	*newfile;
	if (!file)
		return (NULL);
	if (!(newfile = malloc(sizeof(t_file))))
		return (newfile);
	newfile->name = ft_strdup(file->name);
	newfile->st = file->st;
	return (newfile);
}

void	free_file(t_file *file)
{
	if (!file)
		return;
	if (file->name)
		free(file->name);
	free(file);
}

void add_file(char *dir, t_file ***array, char *name)
{
	int i;
	t_file **newarray;

	i = 0;
	if (*array)
	{
		while ((*array)[i])
			i++;
	}
	newarray = (t_file**)malloc(sizeof(t_file*)*(i + 2));
	if (*array)
	{
		i = 0;
		while ((*array)[i])
		{
			newarray[i] = (*array)[i];
			i++;
		}
		free(*array);
	}
	newarray[i] = new_file(dir, name);
	newarray[i + 1] = NULL;
	*array = newarray;
}

void add_file_copy(t_file ***array, t_file *file)
{
	int i;
	t_file **newarray;

	i = 0;
	if (*array)
	{
		while ((*array)[i])
			i++;
	}
	newarray = (t_file**)malloc(sizeof(t_file*)*(i + 2));
	if (*array)
	{
		i = -1;
		while ((*array)[++i])
			newarray[i] = (*array)[i];
		free(*array);
	}
	newarray[i] = copy_file(file);
	newarray[i + 1] = NULL;
	*array = newarray;
}