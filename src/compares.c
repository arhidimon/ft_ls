/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   compares.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dbezruch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/26 11:28:00 by dbezruch          #+#    #+#             */
/*   Updated: 2018/10/23 14:57:18 by dbezruch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/ft_ls.h"

int cmp_name(t_file *f1, t_file *f2)
{
	return (ft_strcmp(f1->name, f2->name));
}

int cmp_name_r(t_file *f1, t_file *f2)
{
	return (ft_strcmp(f2->name, f1->name));
}

int cmp_size(t_file *f1, t_file *f2)
{
	return (ft_strcmp(f2->name, f1->name));
}