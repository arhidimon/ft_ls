/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_dir.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dbezruch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/26 10:49:00 by dbezruch          #+#    #+#             */
/*   Updated: 2018/10/23 14:57:09 by dbezruch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/ft_ls.h"
#include <stdio.h>
#include <dirent.h>

t_file **get_dir(char *dirname)
{
	DIR *dir;
	struct dirent *entry;
	t_file **files;

	files = NULL;
	dir = opendir(dirname);
	if (!dir)
	{
		perror(P_NAME);
		return (NULL);
	}
	while ((entry = readdir(dir)) != NULL)
		add_file(dirname, &files, entry->d_name);
	closedir(dir);

	return (files);
}