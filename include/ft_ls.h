/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ls.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dbezruch <dbezruch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/12 18:00:00 by dbezruch          #+#    #+#             */
/*   Updated: 2017/12/12 18:00:00 by dbezruch ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#ifndef FT_LS_H
# define FT_LS_H

#define P_NAME "ft_ls"

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"

# include <sys/stat.h>
# include "../libft/libft.h"



typedef struct		s_params
{
	unsigned int  one: 1;
	unsigned int  l: 1;
	unsigned int  r: 1;
	unsigned int  a: 1;
	unsigned int  t: 1;
	unsigned int  u: 1;
	unsigned int  f: 1;
	unsigned int  g: 1;
	unsigned int  d: 1;
	unsigned int  G: 1;
	unsigned int  R: 1;
}					t_params;

typedef struct		s_file
{
	char				*name;
	struct stat			st;
}					t_file;

typedef struct		s_filestr
{
	char				*name;
	char				*perm;
	char 				*links;
	char 				*login;
	char 				*group;
	char 				*size;
}					t_filestr;

typedef struct		s_app
{
	t_params			p;
	int					ac;
	char				**av;
}					t_app;

extern t_app g_a;

int get_options(int argc, char **argv, char *av_opt, char **opt);
void appendstr(char ***array, char *str);
void quicksort_strs(char const *args[], unsigned int len);
void quicksort_files(t_file **args, unsigned int len,int (*cmp)(t_file*, t_file*));
t_file **get_only_visible(t_file **files);
char **get_files_in_dir(char *dirname);
void print_1col(char * curdir, t_file **files);
void recursive_search(char *startdir, char *curdir,t_file **(*get_dir_content)(char *), void (*printfunc)(char *, t_file**));
int get_strarr_len(char **string);
void printlong(char *curdir, t_file **files);
char *get_fullname(char *dir, char *name);
t_file **get_dir(char *dirname);
unsigned int get_num_files(t_file **files);
int cmp_name(t_file *f1, t_file *f2);
void add_file(char *dir, t_file ***array, char *name);
void add_file_copy(t_file ***array, t_file *file);
#endif