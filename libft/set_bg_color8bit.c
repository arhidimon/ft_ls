/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   set_bg_color8bit.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dbezruch <dbezruch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/21 18:28:00 by dbezruch          #+#    #+#             */
/*   Updated: 2017/12/21 18:28:00 by dbezruch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void set_bg_color8bit(char color)
{
	ft_putstr("\x1b[48;5;");
	ft_putnbr(color);
	ft_putchar('m');
}