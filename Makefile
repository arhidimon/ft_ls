NAME        = ft_ls
CC          = gcc
#CFLAGS      = -Wall -Wextra -Werror -O3 -g

SRCS_DIR    = ./src
OBJS_DIR    = ./obj
HEADERS_DIR = ./include
LIBFT_DIR   = ./libft

SRCS        = main.c printlong.c get_file_type.c get_files_in_dir.c get_only_visible.c recursive_search.c print_1col.c get_strarr_len.c get_fullname.c add_file.c compares.c get_dir.c get_num_files.c qsort.c get_options.c appendstr.c

OBJS        = $(SRCS:.c=.o)

VPATH       = $(SRCS_DIR) $(OBJS_DIR)

INCLUDES    = -I include/
INCLUDES   += -I libft/

LIBFT       = $(LIBFT_DIR)/libft.a
LIBRARIES   = $(LIBFT)

TO_LINKING  = $(addprefix $(OBJS_DIR)/, $(OBJS)) $(INCLUDES) $(LIBRARIES) -rpath frameworks/

all         : $(NAME)

$(NAME)     : $(LIBFT) $(LIBJSON) $(OBJS_DIR) $(OBJS) $(HEADERS)
	@$(CC) $(CFLAGS) -o $(NAME) $(TO_LINKING)
	@printf "\e[38;5;46m./$(NAME)   SUCCESSFUL BUILD 🖥\e[0m\n"
	@printf "\e[38;5;5m"
	@echo " ███████╗████████╗     ██╗     ███████╗            ██████╗ ██╗   ██╗ "
	@echo " ██╔════╝╚══██╔══╝     ██║     ██╔════╝            ██╔══██╗╚██╗ ██╔╝ "
	@echo " █████╗     ██║        ██║     ███████╗            ██████╔╝ ╚████╔╝  "
	@echo " ██╔══╝     ██║        ██║     ╚════██║            ██╔══██╗  ╚██╔╝   "
	@echo " ██║        ██║███████╗███████╗███████║            ██████╔╝   ██║    "
	@echo " ╚═╝        ╚═╝╚══════╝╚══════╝╚══════╝            ╚═════╝    ╚═╝    "
	@echo "                                                                    "
	@echo "  █████╗ ██████╗ ██╗  ██╗██╗██████╗ ██╗███╗   ███╗ ██████╗ ███╗   ██╗"
	@echo " ██╔══██╗██╔══██╗██║  ██║██║██╔══██╗██║████╗ ████║██╔═══██╗████╗  ██║"
	@echo " ███████║██████╔╝███████║██║██║  ██║██║██╔████╔██║██║   ██║██╔██╗ ██║"
	@echo " ██╔══██║██╔══██╗██╔══██║██║██║  ██║██║██║╚██╔╝██║██║   ██║██║╚██╗██║"
	@echo " ██║  ██║██║  ██║██║  ██║██║██████╔╝██║██║ ╚═╝ ██║╚██████╔╝██║ ╚████║"
	@echo " ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝╚═╝╚═════╝ ╚═╝╚═╝     ╚═╝ ╚═════╝ ╚═╝  ╚═══╝"
	@echo "                                                                    "
	@printf "\e[0m\n"




$(LIBFT)    :
	@make -C $(LIBFT_DIR)

$(LIBJSON)  :
	@make -C $(LIBJSON_DIR)

$(OBJS_DIR) :
	@mkdir $(OBJS_DIR)
	@printf "\e[38;5;46m$(OBJS_DIR)    FOLDER CREATED\e[0m\n"

$(OBJS)     : %.o : %.c $(HEADERS)
	@$(CC) $(CFLAGS) -c $< -o $(OBJS_DIR)/$@ $(INCLUDES)

clean       :
	@rm -rf $(OBJS_DIR)
	@make -C $(LIBFT_DIR) clean
	@printf "\e[38;5;226m$(OBJS_DIR)    FOLDER DELETED\e[0m\n"

fclean      : clean
	@rm -f $(NAME)
	@make -C $(LIBFT_DIR) fclean
	@printf "\e[38;5;226m./$(NAME)   DELETED\e[0m\n"

re          : fclean all

norm		:
	norminette src/ includes/

leaks       :
	leaks $(NAME)

test		:
	make
	./$(NAME) 4

author		:
	cat -e author

.PHONY: clean fclean re author
